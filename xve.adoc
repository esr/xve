= xve(1) =
:doctype: manpage

== NAME ==
xve - X Video Explorer

== SYNOPSIS ==
*xve*

[[description]]
== DESCRIPTION ==

X Video Explorer automates the computations described in the "XFree86
Video Timings HOWTO:. This program is meant to get you started if you
have no functioning mode line at all; you can then tweak the result
with xvidtune(1).

To run it, simply invoke xve. The program's messages should be
self-explanatory.  Help is available at any command prompt by typing
"?".

[[files]]
== FILES ==
~/.xve::
    saved profile from your last configuration run

[[bugs]]
== BUGS == 
It's not a complete or exact calculator.  The assumptions
made are conservative.

[[author]]
== AUTHOR ==
Eric S. Raymond <esr@thyrsus.com>

// end
